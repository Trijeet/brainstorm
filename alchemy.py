#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

##Base

##External
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

##Internal
from models import Schema, User, Idea, seedDB
from services import IdeaService

##App declaration:
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/brainstorm.db'
db = SQLAlchemy(app)

##Simple backend for the idea routing app.

@app.route("/")
def hello():
    return "Response for index."

@app.route("/<name>")
def helloSpecify(name):
    return f"Response for index at {name}."
    
@app.route("/idea", methods=["POST"])
def createIdea():
    ##This is the route accepting input from the user.
    ##The default is to parse into JSON and then pass into the database.
    return IdeaService().create(request.get_json())



class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    _is_deleted = db.Column(db.String(120), unique=False, nullable=False, default=False)
    created_on = db.Column(db.String(120), unique=False, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return '<User %r>' % self.username

class Idea(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), unique=True, nullable=False)
    summary = db.Column(db.Text, unique=False, nullable=True)
    thoughts = db.Column(db.Text, unique=False, nullable=True)
    related_thoughts = db.Column(db.Text, unique=False, nullable=True)
    _is_deleted = db.Column(db.String(120), unique=False, nullable=False, default=False)
    created_on = db.Column(db.DateTime, unique=True, nullable=False, default=datetime.utcnow)
    user_id = db.Column(db.Integer(120), db.ForeignKey('user.id'), unique=False, nullable=False)
    user = db.relationship('User',
        backref=db.backref('ideas', lazy=True))

    def __repr__(self):
        return '<title %r>' % self.title


def seedDB(db):
    '''Default user and data for the database.'''
    u1 = User(username='admin', email='admin@example.com')
    id1 = Idea(
        title="Test", summary="This is a summary.", thoughts="I have no thoughts.", related_thoughts="This is a test citation to be linked.",
        user=u1
    )
    db.session.add(u1)
    db.session.commit()
    User.query.all()
