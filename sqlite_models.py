import sqlite3

##Internal
class IdeaModel:
    TABLENAME = "IDEAS"
    
    def __init__(self, db_path):
        self.conn = sqlite3.connect(db_path)
        self.create_idea_table()  
    
    def createOne(self, title, summary, thoughts, related_thoughts, user):
        new_record = (title, summary, thoughts, related_thoughts, user)
        result = self.conn.execute('INSERT INTO ideas (Title, Summary, Thoughts, RelatedThoughts, UserId) VALUES (?,?,?,?,?)', new_record)
        return result

    def createMany(self, list_of_records):
        assert isinstance(list_of_records, list), "Must pass a list of tuples to create many."
        result = self.conn.execute('INSERT INTO ideas (Title, Summary, Thoughts, RelatedThoughts, UserId) VALUES (?,?,?,?,?)', new_record)
        return result
    
    def read(self):
        pass
    
    def update(self):
        pass

    def destroy(self):
        pass

    def create_idea_table(self):
        ##change quotes on Ideas if fails
        ## TO-DO implement Thoughts as a list
        ## TO-DO implement a reference between this id and other Thoughts via RelatedThoughts.
        with open('./schemas.sql', 'r') as f:
            tmp = self.conn.cursor()
            tmp.executescript(f.read())
            tmp.close()

    def seedDB(self):
        ideas = [
            ('title1', 'This is a summary.', 'I have some thoughts.', 'These are the related thoughts.', ''),
            ('title2', 'This is a summary2.', 'I have some thoughts2.', 'These are the related thoughts2.', ''),
            ('title3', 'This is a summary3.', 'I have some thoughts3.', 'These are the related thoughts3.', ''),
        ]
        c.executemany('INSERT INTO ideas (Title, Summary, Thoughts, RelatedThoughts, UserId) VALUES (?,?,?,?,?)', ideas)


class UserModel:
    TABLENAME = "USERS"
    
    def __init__(self, db_path):
        self.conn = sqlite3.connect(db_path)
        self.create_user_table()      
    
    def createOne(self, name, email):
        new_record = (name, email)
        result = self.conn.execute('INSERT INTO users (Name, Email) VALUES (?,?)', users)
        return result

    def createMany(self, list_of_records):
        assert isinstance(list_of_records, list), "Must pass a list of tuples to create many."
        result = self.conn.execute('INSERT INTO users (Name, Email) VALUES (?,?)', users)
        return result
    
    def read(self):
        pass
    
    def update(self):
        pass

    def destroy(self):
        pass

    def create_user_table(self):
        with open('./schemas.sql', 'r') as f:
            tmp = self.conn.cursor()
            tmp.executescript(f.read())
            tmp.close()

    def seedDB(self):
        users = [
            ('admin1', 'test@example.com'),
            ('admin2', 'test2@example.com'),
            ('admin3', 'test3@example.com'),
        ]
        c.executemany('INSERT INTO users (Name, Email) VALUES (?,?)', users)