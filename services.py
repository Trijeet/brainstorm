#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

##Base

##External

##Internal
# from models import IdeaModel, UserModel ##PGSQL
from sqlite_models import IdeaModel, UserModel ##SQLITE


class IdeaService:
    '''Alias for the underlying model in sqlite 3 and validator piece.'''
    def __init__(self, db_path):
        self.model = IdeaModel(db_path)
    
    def createOne(self, params):
        ##Takes data from a post and translates it for the database inserter.
        ##No validation is done, and all arguments are expected to be nicely formatted JSON.
        self.model.createOne(
            title=params["title"],
            summary=params["summary"],
            thoughts=params["thoughts"],
            related_thoughts=params["related_thoughts"]
        )
    
    def read(self):
        pass
    
    def update(self):
        pass

    def destroy(self):
        pass

class UserService:
    '''Alias for the underlying model in sqlite 3 and validator piece.'''
    def __init__(self, db_path):
        self.model = UserModel(db_path)
    
    def createOne(self, params):
        ##Takes data from a post and translates it for the database inserter.
        ##No validation is done, and all arguments are expected to be nicely formatted JSON.
        self.model.createOne(
            title=params["title"],
            summary=params["summary"],
            thoughts=params["thoughts"],
            related_thoughts=params["related_thoughts"]
        )
    
    def read(self):
        pass
    
    def update(self):
        pass

    def destroy(self):
        pass