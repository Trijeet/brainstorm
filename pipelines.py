#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

##Base

##External
from sklearn import MLModel
import vowpalwabbit as vw

##Internal
from .models import IdeaModel

class AnomalyFinder():
    pass

class Bandit():
    pass

class TimeSeriesAnalysis():
    pass