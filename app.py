#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

##Base

##External
from flask import Flask
from flask import g

##Internal
from services import IdeaService, UserService

##Utility Functions
def makeDicts(cursor, row):
    return dict((cursor.description[idx][0], value)
                for idx, value in enumerate(row))

def getDB():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = makeDicts
    return db

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

##App declaration:
DATABASE = './brainstorm.db'
app = Flask(__name__)

##Simple backend for the idea routing app.
@app.route("/")
def hello():
    return "Response for index."

@app.route("/<name>")
def helloSpecify(name):
    return f"Response for index at {name}."
    
@app.route("/idea", methods=["POST"])
def createIdea():
    ##This is the route accepting input from the user.
    ##The default is to parse into JSON and then pass into the database.
    return IdeaService().create(request.get_json())

# @app.route("/todo", methods=["GET"])
# def list_todo():
#     return jsonify(ToDoService().list())


# @app.route("/todo", methods=["POST"])
# def create_todo():
#     return jsonify(ToDoService().create(request.get_json()))


# @app.route("/todo/<item_id>", methods=["PUT"])
# def update_item(item_id):
#     return jsonify(ToDoService().update(item_id, request.get_json()))


# @app.route("/todo/<item_id>", methods=["DELETE"])
# def delete_item(item_id):
#     return jsonify(ToDoService().delete(item_id))

##App context:
@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

if __name__ == "__main__":
    ##Connect to the services:
    US = UserService('./brainstorm.db')
    IS = IdeaService('./brainstorm.db')
    ##Run Flask.
    app.run(debug=True)
