#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

##Base

##External
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


def buildDB():
    conn = psycopg2.connect(
       database="postgres", user='postgres', password='postgres', host='127.0.0.1', port='5432'
    )
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = conn.cursor()
    cursor.execute("SELECT 1 FROM pg_catalog.pg_database WHERE datname = 'brainstorm'")
    exists = cursor.fetchone()
    if not exists:
        cursor.execute('CREATE DATABASE brainstorm')
    conn.close()

def buildTables(conn):
    with open("./Brainstorm/schemas.sql", "r") as f:
        sql = f.read().encode("utf-8")
    with conn.cursor() as curs:
        curs.execute(sql)
        curs.commit()

def startDB():
    conn = psycopg2.connect(
       database="brainstorm", user='postgres', password='postgres', host='127.0.0.1', port='5432'
    )
    return conn

def seedIdeas(conn):

    ideas = [
        ('title1', 'This is a summary.', '{I have some thoughts., Some different thoughts.}', '{These are the related thoughts.}', '5'),
        ('title2', 'This is a summary2.', '{I have some thoughts2., Some different thoughts2.}', '{These are the related thoughts2.}', '6'),
        ('title3', 'This is a summary3.', '{I have some thoughts3., Some different thoughts3.}', '{These are the related thoughts3.}', '5'),
    ]
    sql = 'INSERT INTO ideas (title, summary, thoughts, relatedthoughts, userid) VALUES %s'
    with conn.cursor() as cursor:
        cursor.execute("DROP TABLE IF EXISTS ideas;")
        buildTables()
        execute_values(
            cursor, sql, ideas, template=None, page_size=100
        )
        conn.commit()

def seedUsers(conn):    
    users = [
        ('admin1', 'test@example.com'),
        ('admin2', 'test2@example.com'),
        ('admin3', 'test3@example.com'),
    ]
    sql = 'INSERT INTO users (name, email) VALUES %s'
    with conn.cursor() as cursor:
        cursor.execute("DROP TABLE IF EXISTS users;")
        buildTables()
        execute_values(
            cursor, sql, users, template=None, page_size=100
        )
        conn.commit()

def selectAll(conn):
    with conn.cursor() as cursor:
        cursor.execute("SELECT * from users;")
        res = cursor.fetchall()
    return res

def validateSQL(sql):
    return sql

def reqData(conn, sql):
    sql = validateSQL(sql)
    with conn.cursor() as cursor:
        cursor.execute(sql)
        res = cursor.fetchall()
    return res