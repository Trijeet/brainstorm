CREATE TABLE IF NOT EXISTS Users (
    id serial PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    _is_deleted boolean DEFAULT FALSE,
    createdon TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS Ideas (
    id serial PRIMARY KEY,
    userid INTEGER NOT NULL,
    title TEXT NOT NULL,
    summary TEXT,
    thoughts TEXT[],
    relatedthoughts TEXT[],
    _is_deleted boolean DEFAULT FALSE,
    createdon TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (userid) REFERENCES users (id)
);